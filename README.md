# README #

Wurm Unlimited Adaptor Server to attempt to securely serve sqlite internals, in an immutable way.
Employs rate-limiting and helmet for greater effect.

### Examples Implemented ###

Stat 1 = creature count.

```
GET localhost:8080/stat/1

Response:

{
  "stat": 1,
  "value": 12587
}
```