'use strict';

const config = require('./config');
const winston = require('winston');

/**
 * Helper Assistant Class
 */
class Assist {

    constructor() {
        this.logger = this.getLogger();
    }

    /**
     * Express-rate-limit callback/handler
     */
    handleRateLimitExceeded(req, res, next) {
        let status = config.ratelimit.status;
        let message = config.ratelimit.message;

        let logger = new (winston.Logger)({
            transports: [ new (winston.transports.Console)({ colorize: true, timestamp: true }) ]
        });

        /**
         * Specific logger for rate limiting.
         */
        logger.add(winston.transports.File, {
            name: 'ratelimit',
            filename: config.ratelimit.log,
            timestamp: true
        });

        res.format({
            html: () => {
                res.status(status).end(message);
            },
            json: () => {
                res.status(status).json({ message: message });
            }
        });

        const logmeta = {
            src: req.ip,
            path: req.path
        };

        logger.error('rate-limit-exceeded', logmeta);
    }

    getLogger() {
        if (!this.logger) {
            this.logger = new (winston.Logger)({
                transports: [ new (winston.transports.Console)({ colorize: true, timestamp: true }) ]
            });

            this.logger.level = config.loglevel;

            this.logger.add(winston.transports.File, {
                name: 'server',
                filename: config.server.logfile,
                timestamp: true
            });
        }
        return this.logger;
    }
}

module.exports = Assist;
