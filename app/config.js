'use strict';

module.exports = {

    server: {
        port: 8080,
        enabled: true,
        logfile: 'wusec_server.log'
    },
    ratelimit: {
        windowMinutes: 1,   // 1 minute window
        max: 10,    // requests per IP per windowMinutes
        delayMs: 0,   // response delays,
        status: 429,
        message: 'API Rate Limit Exceeded',
        log: 'rate-limit.log'
    },
    loglevel: 'debug',
    appname: 'WuSecServ',

    /**
     * Specify sqlite db locations here.
     */
    databases: {
        creatures: '/Users/marlon/tempwu/wurmcreatures.db'
    }
};
