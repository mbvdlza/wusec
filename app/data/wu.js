'use strict';

let fs = require('fs');
let sqlite3 = require('sqlite3');

/**
 * Adaptor to sqlite databases used by wurm.
 * The intention here is to only make simple, json based READS available.
 * If you add mutators, you do so at your own risk, and at the peril of your players.
 */
class WU {

    /**
     * Instantiates an instance of WU
     * @param logger instance of logger (ie winston)
     */
    constructor(logger, config) {
        this.logger = logger;
        this.config = config;
    }

    open(dbfile) {
        return new Promise((resolve, reject) => {
            if (fs.existsSync(dbfile)) {
                let db = new sqlite3.Database(dbfile, sqlite3.OPEN_READONLY, (err) => {
                    if (err) {
                        reject('Opening %s was a problem.', dbfile);
                    }
                    resolve(db);
                });
            } else {
                this.logger.error('creaturesDatabase %s is not readable', creaturesDatabase);
                reject('creaturesDatabase %s is not readable', creaturesDatabase);
            }

        });

    }

    /**
     * Gets the number of creatures from CREATURES table in the wurmcreatures database.
     * @returns {Promise}
     */
    getNumCreature() {
        let creaturesDatabase = this.config.databases.creatures;

        return new Promise((resolve, reject) => {
            this.open(creaturesDatabase).then((db) => {
                db.get('SELECT COUNT(WURMID) AS count FROM CREATURES', {}, (err, row) => {
                    if (!err && row.count) {
                        resolve(parseInt(row.count));
                    }
                    resolve(-1);
                });
            }).catch((err) => {
                this.logger.error('getNumCreature error %s', err);
                reject(err);
            });
        });
    }


}

module.exports = WU;
