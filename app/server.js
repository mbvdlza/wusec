'use strict';

let Assist = require('./assist');
let express = require('express');
let helmet = require('helmet');
let RateLimit = require('express-rate-limit');
let WU = require('./data/wu');

let assist = new Assist();
let logger = assist.getLogger();

const config = require('./config');

let wu = new WU(logger, config);

let limiter = new RateLimit({
    windowMs: config.ratelimit.windowMinutes * 60 * 1000,   // config mins to millis
    max: config.ratelimit.max,
    delayMs: config.ratelimit.delayMs,
    message: 'API Rate Limit Exceeded',
    handler: assist.handleRateLimitExceeded
});

let app = express();
app.use(helmet());
app.use(limiter);   // limiter on all routes

app.get('/', (req, res) => {
    logger.info('GET / -> from %s', req.ip);
    res.send({ message: 'Improper use of this API' });
});

// quickly defining the default objects here... will clean this up.
let resJson = {
    stat: null,
    value: null
};

let errJson = {
    error: null
};

app.get('/stat/:id', (req, res) =>{
    logger.info('GET /stat/%d -> from %s', req.params.id, req.ip);
    const stat = parseInt(req.params.id);

    switch (stat) {
        case(1):
            resJson.stat = stat;

            wu.getNumCreature().then((count) => {
                resJson.value = count;
                res.send(resJson);
            }).catch((err) => {
                // 'Error... deal with this. Correct status code etc...
                res.status(500).send(err.toString());
            });

            break;
        default:
            errJson.error = 'unknown stat';
            res.status(404).send(errJson);
    }

});

app.listen(config.server.port, () => {
    logger.info('%s listening on %d!',config.appname, config.server.port)
});
